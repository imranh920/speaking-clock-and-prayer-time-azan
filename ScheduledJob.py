import tkinter as tk
from tkinter import *
from tkinter import scrolledtext
import requests
import threading
import time
import datetime
from playsound import playsound

class MinuteHour:
    def __init__(self):
        self.PrayerName = ""
        self.Hour = 0
        self.Minute = 0
class PrayDate:
    def __init__(self):
        self.month = ''
        self.day = ''
        self.times = []
    
URL = "http://api.aladhan.com/v1/calendar"

lastDate = -2

dailyPrayers = ['Fajr','Dhuhr','Asr','Maghrib','Isha']

# http://api.aladhan.com/v1/calendar?latitude=23.8103&longitude=90.4125&annual=true&school=1

latitude = '23.8103'
longidude = '90.4125'
PARAMS = {'latitude':latitude,'longitude':longidude,'annual':'true','school':'1'}

window = tk.Tk()
txt = scrolledtext.ScrolledText(window,width=200,height=30)
timeList = []
longEntry = tk.Entry(window)
latEntry = tk.Entry(window)

def fetchHourMinuteFromTime(tim, name):
    prayTime = tim[name]
    pTime = MinuteHour()
    pTime.PrayerName = name
    tm = prayTime.split()[0]
    pTime.Hour = int(tm.split(':')[0])
    pTime.Minute = int(tm.split(':')[1])
    return pTime

def fetchDataFromApi():
    longidude = longEntry.get()
    latitude = latEntry.get()
    r = requests.get(url = URL, params = PARAMS)
    data = r.json() 
    
    for mnt in data['data']:
        print('Month - ')
        print('\n')
        monthDay = 0
        for dy in data['data'][mnt]:
            monthDay = monthDay + 1
            tim = dy['timings']
            pDate = PrayDate()
            pDate.month = mnt
            pDate.day = monthDay
            for p in dailyPrayers:
                pDate.times.append(fetchHourMinuteFromTime(tim, p))
            timeList.append(pDate);

def startProcess():
    fetchDataFromApi()
    t1 = threading.Thread(target=timerThread, args=[])
    t1.start()

def createGui(window):
    window.title("Sagar Fishing Net Industries")
    lbl1 = tk.Label(window,text='Longitude')
    lbl2 = tk.Label(window,text='Latitude')
    lbl1.grid(column=0,row=0)
    lbl2.grid(column=0,row=1)
    longEntry.grid(column=1,row=0)
    latEntry.grid(column=1,row=1)
    colNum = 3
    for p in dailyPrayers:
        pLbl = tk.Label(window,text=p)
        pLbl.grid(row = 0, column = colNum)
        colNum = colNum + 1
    tk.Button(window, text='Fetch Data and Start', command=startProcess).grid(row=2, column=0, sticky=tk.W, pady=4)
    txt.grid(column=0,row=3,columnspan=15)
    return txt

def playSoundFile(fileName, block):
    playsound('D:\Times\/' + fileName +'.wav', block=block)

def playAzan():
    global lastDate
    colNum = 3
    currentDT = datetime.datetime.now()
    thisDay = next(x for x in timeList if (x.day == currentDT.day and x.month == str(currentDT.month)))
    if lastDate != currentDT.day:
        lastDate = currentDT.day
        for p in thisDay.times:
            pLbl = tk.Label(window,text=str(p.Hour) + ':' + str(p.Minute))
            pLbl.grid(row = 1, column = colNum)
            colNum = colNum + 1
    prayTime = next((x for x in thisDay.times if(x.Hour == currentDT.hour and x.Minute == currentDT.minute)), None)
    if prayTime is not None:
        playSoundFile('azan', True)


def playTime():
    currentDT = datetime.datetime.now()
    timeFileName = ''
    hour = currentDT.hour % 12
    if hour == 0:
        hour = 12
    if currentDT.hour >= 0 and currentDT.hour <= 4:
        timeFileName = 'raat'
    if currentDT.hour == 5:
        timeFileName = 'vor'
    if currentDT.hour >= 6 and currentDT.hour <= 11:
        timeFileName = 'sokal'
    if currentDT.hour >= 12 and currentDT.hour <= 15:
        timeFileName = 'dupur'
    if currentDT.hour >= 16 and currentDT.hour <= 17:
        timeFileName = 'bikal'
    if currentDT.hour >= 18 and currentDT.hour <= 19:
        timeFileName = 'sondha'
    if currentDT.hour >= 20 and currentDT.hour <= 24:
        timeFileName = 'raat'
    
    if timeFileName != '':
        playSoundFile('chime', True)
        playSoundFile('ekhon_somoy', True)
        playSoundFile(timeFileName, True)
        playSoundFile(str(hour), True)

def timerThread():
    while True:
        currentDT = datetime.datetime.now()
        t2 = threading.Thread(target=playAzan, args=[])
        t2.start()
        t3 = threading.Thread(target=playTime, args=[])
        t3.start()
        txt.insert(INSERT, '\nPlaying at ' + str(currentDT.minute) + "  " + str(currentDT.second))
        
        time.sleep(60)

def main():
    longEntry.insert(INSERT, longidude)
    latEntry.insert(INSERT, latitude)
    createGui(window)
    window.mainloop()

main()